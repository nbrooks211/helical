CC=cc
CFLAGS=-std=c99 -Wall -Wpedantic
CURL=-lcurl
LIBS=$(CURL) -ljson-c
OPT=-O3
DESTDIR=/usr
VER=0.0.3
SVER=0

S=libhelical.so
SO=$(S).$(VER)
A=libhelical.a
H=helical.h


build: $(SO)

$(SO):
	$(CC) libhelical.c -o $(SO) $(CFLAGS) -shared $(LIBS) $(OPT)

$(A):
	$(CC) libhelical.c -o $(A) $(CFLAGS) -static $(LIBS) $(OPT)



debug: $(SO)_debug

$(SO)_debug:
	$(CC) libhelical.c -o $(SO) $(CFLAGS) -shared $(LIBS) -ggdb

$(A)_debug:
	$(CC) libhelical.c -o $(A) $(CFLAGS) -static $(LIBS) -ggdb


static: $(A)



$(DESTDIR)/lib/$(SO):
	mkdir -p $(DESTDIR)/lib/
	cp -f $(SO) $(DESTDIR)/lib/
	ln -s $(SO) $(DESTDIR)/lib/$(S).$(SVER)
	ln -s $(SO) $(DESTDIR)/lib/$(S)

$(DESTDIR)/lib/$(A):
	mkdir -p $(DESTDIR)/lib/
	cp -f $(A) $(DESTDIR)/lib/

$(DESTDIR)/include/helical/$(H):
	mkdir -p $(DESTDIR)/include/helical/
	cp -f $(H) $(DESTDIR)/include/helical/



$(SO)_debug_fullinstall: uninstall $(SO)_debug $(DESTDIR)/lib/$(SO) $(DESTDIR)/include/helical/$(H)

$(A)_debug_fullinstall: uninstall $(A)_debug $(DESTDIR)/lib/$(A) $(DESTDIR)/include/helical/$(H)

$(SO)_fullinstall: uninstall $(SO) $(DESTDIR)/lib/$(SO) $(DESTDIR)/include/helical/$(H)

$(A)_fullinstall: uninstall $(A) $(DESTDIR)/lib/$(A) $(DESTDIR)/include/helical/$(H)

installstatic: $(A)_fullinstall

install: $(SO)_fullinstall

installdebug: $(SO)_debug_fullinstall



clean: cleantest
	rm -f $(S)*
	rm -f $(A)


$(H)_uninstall:
	rm -fr $(DESTDIR)/include/helical/

$(SO)_uninstall:
	rm -f $(DESTDIR)/lib/$(SO)
	rm -f $(DESTDIR)/lib/$(S).$(SVER)
	rm -f $(DESTDIR)/lib/$(S)

$(A)_uninstall:
	rm -f $(DESTDIR)/lib/$(A)

uninstall: clean $(H)_uninstall $(SO)_uninstall $(A)_uninstall



$(A)_test:
	$(MAKE) -C tests check_base_url_static
#	$(MAKE) -C tests get_access_token_static

$(SO)_test:
	$(MAKE) -C tests check_base_url
#	$(MAKE) -C tests get_access_token

statictest: $(A)_test

test: $(SO)_test

cleantest:
	$(MAKE) -C tests clean
