/*
* Copyright (C) 2022 Aleksandar Widulski
*
* This file is part of Helical
*
* Helical is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as
* published by the Free Software Foundation, either version 3 of the
* License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
* You should have received a copy of the GNU Affero General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#define HELICAL_E_NOJSON 1
#define HELICAL_E_NOMEM 2
#define HELICAL_E_MERR 3
#define HELICAL_VER "0.0.3"

static struct merror {
	char *errcode;
	char *error;
	int real;
};

struct merror get_matrix_error(char *json);
int check_base_url(char *url, char **base_url);
int get_access_token(char *base_url, char *user, char *pass, char **token);
int send_event(char *base_url, char *room_id, char *event, char *content, char *nonce, char *access_token, char **ptr);
