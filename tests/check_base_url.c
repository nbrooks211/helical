#define _POSIX_C_SOURCE 200809L

#include <stdlib.h>
#include <stdio.h>

#include <curl/curl.h>
#include "../helical.h"

int main()
{
	curl_global_init(CURL_GLOBAL_ALL);

	char *base_url = malloc(1);
	check_base_url("matrix.org", &base_url);
	printf("%s\n", base_url);

	curl_global_cleanup();
	free(base_url);
	return 0;
}
