/*
* Copyright (C) 2022 Aleksandar Widulski
*
* This file is part of Helical
*
* Helical is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as
* published by the Free Software Foundation, either version 3 of the
* License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
* You should have received a copy of the GNU Affero General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#define _POSIX_C_SOURCE 200809L

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <curl/curl.h>
#include <json-c/json.h>

#include "helical.h"

static struct content {
	char *content;
	size_t size;
};

// Handle matrix errors without using a JSON LIbrary
struct merror get_matrix_error(char *json) {
	struct merror err;
	json_object *root = json_tokener_parse(json);
	// JSON is invalid
	if (!root) {
		err.real = HELICAL_E_NOJSON;
		return err;
	}

	json_object *errcode = json_object_object_get(root, "errcode");
	json_object *error = json_object_object_get(root, "error");
	while (json_object_put(root) != 1) {}

	// No error or no error code in given JSON
	if (!(errcode && error)) {
		err.real = 8;
		return err;
	}

	err.errcode = json_object_get_string(errcode);
	err.error = json_object_get_string(error);
	return err;
}

static size_t write_callback(void *buffer, size_t size, size_t nmemb, void *userp)
{
	size_t real_size = size * nmemb;
	struct content *cnt = (struct content *)userp;
	char *ptr = realloc(cnt->content, cnt->size + real_size + 1);

	// Return if realloc fail
	if (!ptr) return 0;

	cnt->content = ptr;
	memcpy(&(cnt->content[cnt->size]), buffer, real_size);
	cnt->size += real_size;
	cnt->content[cnt->size] = 0;

	return real_size;
}

int check_base_url(char *raw_url, char **base_url)
{
	// Initialize default return value
	int ret = 0;

	// Length of raw_url including null termination
	const size_t raw_url_len = strlen(raw_url) + 1;

	// Matrix only supports HTTPS as of now
	const char *protocol = "https://";
	const size_t protocol_len = strlen(protocol);
	// Allocate url and variable holding JSON response
	char *url = malloc(1);

	struct content chunk;
	// Allocate to curl response 
	chunk.content = malloc(1);
	if (!chunk.content) {
		ret = HELICAL_E_NOMEM;
		goto cleanup;
	}
	chunk.size = 0;

	if (!url) {
		ret = HELICAL_E_NOMEM;
		goto cleanup;
	}
	size_t url_len = 0;

	// If https:// wasn't given, add it
	if (strncmp(raw_url, protocol, protocol_len)) {
		url = realloc(url, protocol_len + raw_url_len);
		if (!url) {
			ret = HELICAL_E_NOMEM;
			goto cleanup;
		}

		url_len = protocol_len + raw_url_len;
		sprintf(url, "%s%s", protocol, raw_url);
	} else {
		url = realloc(url, raw_url_len);
		if (!url) {
			ret = HELICAL_E_NOMEM;
			goto cleanup;
		}

		url_len = raw_url_len;
		url = raw_url;
	}

	// Allocate and set full path
	url = realloc(url, url_len + 26);
	if (!url) {
		ret = HELICAL_E_NOMEM;
		goto cleanup;
	}

	strcat(url, "/.well-known/matrix/client");

	CURL *curl = curl_easy_init();

	// Set basic curl options
	curl_easy_setopt(curl, CURLOPT_URL, url);
	curl_easy_setopt(curl, CURLOPT_HTTPGET, 1);
	curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1);
	curl_easy_setopt(curl, CURLOPT_FAILONERROR, 1);

	// Write response to write_callback
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_callback);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &chunk);

	CURLcode result = curl_easy_perform(curl);

	if (result == CURLE_OK) {
		json_object *root = json_tokener_parse(chunk.content);
		// If JSON is invalid
		if (!root) {
			ret = HELICAL_E_NOJSON;
			goto cleanup;
		}

		json_object *homeserver = json_object_object_get(root, "m.homeserver");
		json_object *json_base_url = json_object_object_get(homeserver, "base_url");
		// JSON is valid but doesn't contain base url
		if (!json_base_url) {
			// Must be Matrix error
			ret = HELICAL_E_MERR;

			char *json_string = json_object_get_string(root);
			size_t json_string_len = strlen(json_string) + 1;

			*base_url = realloc(*base_url, json_string_len);
			if (!base_url) {
				ret = HELICAL_E_NOMEM;
				goto cleanup;
			}
			// Point base_url to error JSON
			*base_url = memcpy(*base_url, json_string, json_string_len);
			goto cleanup;
		}
		// Otherwise point base_url to base url
		char *json_string = json_object_get_string(json_base_url);
		size_t json_string_len = strlen(json_string) + 1;

		*base_url = realloc(*base_url, json_string_len);
		if (!base_url) {
			ret = HELICAL_E_NOMEM;
			goto cleanup;
		}

		*base_url = memcpy(*base_url, json_string, json_string_len);

		// Clean up JSON objects
		while (json_object_put(homeserver) != 1) {}
		while (json_object_put(root) != 1) {}
	} else {
		// Return curl result + 128
		ret = result + 128;
	}

	curl_easy_cleanup(curl);

cleanup:
	if (url) free(url);
	if (chunk.content) free(chunk.content);

	return ret;
}

int get_access_token(char *base_url, char *user, char *pass, char **token)
{
	// Initialize default return value
	int ret = 0;
	// sprintf() format
	char *format = "{\"type\":\"m.login.password\", \"user\":\"%s\", \"password\":\"%s\"}";
	// JSON to POST
	char *json = malloc(1);

	//TODO: use string builder to escape strings

	size_t base_url_len = strlen(base_url);

	// API login path
	char *path = "/_matrix/client/r0/login";
	size_t path_len = strlen(path);
	// Initialize POST headers
	struct curl_slist *headers = NULL;
	headers = curl_slist_append(headers, "Content-Type: application/json");
	// Allocate to user-agent and URL
	char *ua = malloc(1);
	char *url = malloc(1);

	struct content chunk;
	// Allocate to curl response 
	chunk.content = malloc(1);
	if (!chunk.content) {
		ret = HELICAL_E_NOMEM;
		goto cleanup;
	}
	chunk.size = 0;

	if (!url) {
		ret = HELICAL_E_NOMEM;
		goto cleanup;
	}

	// Allocate correct URL length
	url = realloc(url, base_url_len + path_len + 1);
	if (!url) {
		ret = HELICAL_E_NOMEM;
		goto cleanup;
	}
	// Set URL to homeserver login URL
	sprintf(url, "%s%s", base_url, path);

	if (!json) {
		ret = HELICAL_E_NOMEM;
		goto cleanup;
	}

	// Allocate memory to POST JSON (%s takes up extra space, so subtract 3)
	json = realloc(json, strlen(format) + strlen(user/*_esc*/) + strlen(pass/*_esc*/) - 3);
	if (!json) {
		ret = HELICAL_E_NOMEM;
		goto cleanup;
	}
	// Set POST JSON
	sprintf(json, format, user/*_esc*/, pass/*_esc*/);

	CURL *curl = curl_easy_init();

	// Set basic curl options
	curl_easy_setopt(curl, CURLOPT_URL, url);
	curl_easy_setopt(curl, CURLOPT_HTTPPOST, 1);
	curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1);
	curl_easy_setopt(curl, CURLOPT_FAILONERROR, 1);

	// CA bundle needed for POST
	// TODO: Put CA bundle into memory https://curl.se/libcurl/c/cacertinmem.html
//	curl_easy_setopt(curl, CURLOPT_CAINFO, "/tmp/curl-ca-bundle.crt");

	// Set header option
	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);

	// Allocate and set user-agent 
	ua = realloc(ua, strlen(HELICAL_VER) + 12);
	if (!ua) {
		ret = HELICAL_E_NOMEM;
		goto cleanup;
	}
	sprintf(ua, "libhelical/%s", HELICAL_VER);
	curl_easy_setopt(curl, CURLOPT_USERAGENT, ua);


	// POST json
	curl_easy_setopt(curl, CURLOPT_POSTFIELDS, json);
	curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, -1L);


	// Write response to write_callback
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_callback);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &chunk);

	CURLcode result = curl_easy_perform(curl);

	if (result == CURLE_OK) {
		json_object *root = json_tokener_parse(chunk.content);
		// If JSON is invalid
		if (!root) {
			ret = HELICAL_E_NOJSON;
			goto cleanup;
		}

		json_object *access_token = json_object_object_get(root, "access_token");
		// JSON is valid but doesn't contain access token
		if (!access_token) {
			// Must be Matrix error
			ret = HELICAL_E_MERR;

			char *json_string = json_object_get_string(root);
			size_t json_string_len = strlen(json_string) + 1;

			*token = realloc(*token, json_string_len);
			if (!token) {
				ret = HELICAL_E_NOMEM;
				goto cleanup;
			}
			// Point token to error JSON
			*token = memcpy(*token, json_string, json_string_len);
			goto cleanup;
		}
		// Otherwise point token to access token
		char *json_string = json_object_get_string(access_token);
		size_t json_string_len = strlen(json_string) + 1;

		*token = realloc(*token, json_string_len);
		if (!token) {
			ret = HELICAL_E_NOMEM;
			goto cleanup;
		}

		*token = memcpy(*token, json_string, json_string_len);

		// Clean up JSON objects
		while (json_object_put(root) != 1) {}
	} else {
		// Return curl result + 128
		ret = result + 128;
	}

	curl_easy_cleanup(curl);

cleanup:
	if (headers) curl_slist_free_all(headers);
	if (json) free(json);
	if (url) free(url);
	if (chunk.content) free(chunk.content);
	if (ua) free(ua);

	return ret;
}


int send_event(char *base_url, char *room_id, char *event, char *content, char *nonce, char *access_token, char **ptr)
{
	// Initialize default return value
	int ret = 0;

	size_t base_url_len = strlen(base_url);
	size_t nonce_len = strlen(nonce);

	// API event path
	char *path_format;
	char *path = malloc(1);
	if (!path) {
		ret = HELICAL_E_NOMEM;
		goto cleanup;
	}
	size_t path_len = 0;

	if (nonce_len > 0) {
		path_format = "/_matrix/client/r0/rooms/%s/send/%s/%s";
		path = realloc(path, strlen(path_format) + strlen(room_id) + strlen(event) + nonce_len - 2);
		if (!path) {
			ret = HELICAL_E_NOMEM;
			goto cleanup;
		}
		sprintf(path, path_format, room_id, event, nonce);
	} else {
		path_format = "/_matrix/client/r0/rooms/%s/send/%s";
		path = realloc(path, strlen(path_format) + strlen(room_id) + strlen(event) - 2);
		if (!path) {
			ret = HELICAL_E_NOMEM;
			goto cleanup;
		}
		sprintf(path, path_format, room_id, event, nonce);
	}
	path_len = strlen(path);

	// Initialize POST headers
	struct curl_slist *headers = NULL;
	headers = curl_slist_append(headers, "Content-Type: application/json");
	char *auth_header = malloc(strlen(access_token) + 23);
	if (!auth_header) {
		ret = HELICAL_E_NOMEM;
		goto cleanup;
	}
	sprintf(auth_header, "Authorization: Bearer %s", access_token);
	headers = curl_slist_append(headers, auth_header);
	// Allocate to user-agent and URL
	char *ua = malloc(1);
	char *url = malloc(1);

	struct content chunk;
	// Allocate to curl response 
	chunk.content = malloc(1);
	if (!chunk.content) {
		ret = HELICAL_E_NOMEM;
		goto cleanup;
	}
	chunk.size = 0;

	if (!url) {
		ret = HELICAL_E_NOMEM;
		goto cleanup;
	}

	// Allocate correct URL length
	url = realloc(url, base_url_len + path_len + 1);
	if (!url) {
		ret = HELICAL_E_NOMEM;
		goto cleanup;
	}
	// Set URL to homeserver login URL
	sprintf(url, "%s%s", base_url, path);

	CURL *curl = curl_easy_init();

	// Set basic curl options
	curl_easy_setopt(curl, CURLOPT_URL, url);
	curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1);
	curl_easy_setopt(curl, CURLOPT_FAILONERROR, 1);

	// CA bundle needed for POST
	// TODO: Put CA bundle into memory https://curl.se/libcurl/c/cacertinmem.html
//	curl_easy_setopt(curl, CURLOPT_CAINFO, "/tmp/curl-ca-bundle.crt");

	// Set header option
	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);

	// Allocate and set user-agent 
	ua = realloc(ua, strlen(HELICAL_VER) + 12);
	if (!ua) {
		ret = HELICAL_E_NOMEM;
		goto cleanup;
	}
	sprintf(ua, "libhelical/%s", HELICAL_VER);
	curl_easy_setopt(curl, CURLOPT_USERAGENT, ua);

	// POST json
	curl_easy_setopt(curl, CURLOPT_POSTFIELDS, content);
	curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, -1L);

	// Problematic but the only way to do this AFAIK
	curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "PUT");

	// Write response to write_callback
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_callback);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &chunk);

	CURLcode result = curl_easy_perform(curl);

	curl_easy_cleanup(curl);

	if (result == CURLE_OK) {
		json_object *root = json_tokener_parse(chunk.content);
		// If JSON is invalid
		if (!root) {
			ret = HELICAL_E_NOJSON;
			goto cleanup;
		}

		// Let parent function handle JSON
		char *json_string = json_object_get_string(root);
		size_t json_string_len = strlen(json_string) + 1;

		*ptr = realloc(*ptr, json_string_len);
		if (!ptr) {
			ret = HELICAL_E_NOMEM;
			goto cleanup;
		}

		*ptr = memcpy(*ptr, json_string, json_string_len);

		// Clean up JSON objects
		while (json_object_put(root) != 1) {}
	} else {
		// Return curl result + 128
		ret = result + 128;
	}

cleanup:
	if (headers) curl_slist_free_all(headers);
	if (url) free(url);
	if (chunk.content) free(chunk.content);
	if (ua) free(ua);
	if (path) free(path);
	if (auth_header) free(auth_header);

	return ret;
}
